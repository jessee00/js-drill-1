
function lastCarDetails(inventory)
{
    if((inventory != undefined) && (inventory.length !=0 ))
    {
      let lastCarIndex = inventory.length-1;
        return inventory[lastCarIndex];   // object sent
    }
    else{
        return [];                       // empty array sent
    }
}
module.exports = lastCarDetails;
