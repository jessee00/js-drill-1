
function cars_older_20s(arr_yrs)
{
  if((arr_yrs != undefined) && (arr_yrs.length != 0))
  {
    let count = 0;
    for(let index=0;index<arr_yrs.length;index++)
    {
      if(arr_yrs[index] < 2000) count++;
    }
    return count;
  }
  else{
    return [];
  }
}
module.exports = cars_older_20s;