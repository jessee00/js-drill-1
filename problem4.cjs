
function cars_yrs(inventory)
{ 
   if((inventory != undefined) && (inventory.length != 0))
   {
    let arr_yrs = [];
    for(let index=0;index<inventory.length;index++)
    {
        arr_yrs.push(inventory[index].car_year);
     } 
      return arr_yrs;
   }
   else{
     return [];
   }
}
module.exports = cars_yrs;
