
function CarDetailsSorted(inventory)
{
  if((inventory != undefined) && (inventory.length != 0))
  {
    let car_models = [];
    for(let index=0; index<inventory.length; index++){
        car_models.push(inventory[index].car_model)
    }
    let sorted_models = car_models.sort()
    return sorted_models
  }
  else{
    return []
  } 
}

module.exports = CarDetailsSorted;