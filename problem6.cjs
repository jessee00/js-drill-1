
function car_BMW_AUDI(inventory)
{
  if((inventory != undefined) && (inventory!= 0))
  {
    let car_array = [];
    for(let index=0;index<inventory.length;index++)
    {
       if(inventory[index].car_make == 'BMW') car_array.push(inventory[index]);
       if(inventory[index].car_make == 'Audi') car_array.push(inventory[index]);
    }
    return car_array;
  }
  else{
     return [];
  }
}
module.exports = car_BMW_AUDI;