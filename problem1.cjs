
function carsById(inventory,idx)
{
   if((idx != undefined) && (inventory != undefined) && Array.isArray(inventory) && (typeof idx == 'number'))
   {
     for (let index = 0;index <inventory.length;index++){
        if(inventory[index].id == idx)
        {
          return inventory[index];
        }
      }
   }
  else{
        return [];
    }
}
module.exports = carsById;